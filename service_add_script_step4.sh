#!/bin/bash
docker exec -it myservice rc-update add local default
docker cp myservice_init.start myservice:/etc/local.d/service_init.start
docker exec -it myservice chmod +x /etc/local.d/service_init.start
