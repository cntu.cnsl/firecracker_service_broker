# firecracker_service_broker

*Motivation*: Creation of firecracker image that can start your service at boot

** Preparation before use
+ This script assume your environment has the binary (executable) and its dependencies

** Project Tree
+ Step1: Prepare a ext4 file system and execute docker container
+ Step2: Add basic packages into docker container and enable tty
+ Step3: Copy the project executable and libs into the container
+ Step4: Add the autoboot script (add reboot at the end of the project for serverless)
+ Step5: Finalize the changes

** Result
+ You will have an alpine-based image for running on the firecracker (or may be on others VM --> not tested yet)
