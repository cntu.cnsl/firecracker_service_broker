#!/bin/bash

############################################################ 
#
# For this part, please input your own files and libraries
# In this example, we copy a file and libraries from a project
# named as OpenTEE
#
############################################################

# docker cp /lib/ vtee:/
# docker cp /opt/OpenTee vtee:/opt/OpenTee
# docker cp /etc/opentee.conf vtee:/etc/opentee.conf
# docker cp vtee_init.start vtee:/etc/local.d/vtee_init.start
