#!/bin/bash
docker exec -it myservice apk add openrc
docker exec -it myservice apk add util-linux

docker exec -it myservice ln -s agetty /etc/init.d/agetty.ttyS0
docker exec -it myservice echo ttyS0 > /etc/securetty
docker exec -it myservice rc-update add agetty.ttyS0 default

docker exec -it myservice rc-update add devfs boot
docker exec -it myservice rc-update add procfs boot
docker exec -it myservice rc-update add sysfs boot
