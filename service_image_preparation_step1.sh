#!/bin/bash
dd if=/dev/zero of=rootfs.ext4 bs=1M count=200
mkfs.ext4 rootfs.ext4
mkdir /tmp/my-rootfs
sudo mount rootfs.ext4 /tmp/my-rootfs
## Docker run
docker run --rm -it --name myservice -v /tmp/my-rootfs:/my-rootfs alpine /bin/sh
